const Todo = require('../models/todo.models')

const createTodo = async(req, res)=>{
    todo = new Todo({
        title: req.body.title,
        description: req.body.description
    })

    try {
        const savedTodo = await todo.save()
        return res.status(200).json(savedTodo)
        
    } catch (err) {
        return res.status(500).json(err)
    }
}

const getTodos = async(req, res)=>{

    try {
        const todos = await Todo.find()
        return res.status(200).json(todos)
    } catch (err) {
        return res.status(500).json(err)
    }
}
const getTodo = async(req, res)=>{

    const id = req.params.id
    try {
        const todo = await Todo.findById(id)
        if(!todo) return res.status(404).json('Not Found')
        return res.status(200).json(todo)
    } catch (err) {
        return res.status(500).json(err)
    }
}

const deleteTodo = async(req, res)=> {

    const id = req.params.id

    try {
        const todo = await Todo.findByIdAndDelete(id)
        if(!todo) return res.status(404).json('Not Found')
        return res.status(200).json(todo)
    } catch (err) {
        return res.status(500).json(err)
    }

}

const updateTodo = async(req, res)=>{
    const id = req.params.id

    try {
        const databody = req.body
        const {...updatedData} = databody
        const updatedTodo = await Todo.findByIdAndUpdate(id, updatedData, {new: true})
        if(!todo) return res.status(404).json('Not Found')
        return res.status(200).json(updatedTodo)
    } catch (err) {
        return res.status(500).json(err)
    }
}
module.exports.createTodo = createTodo
module.exports.getTodos = getTodos
module.exports.getTodo = getTodo
module.exports.deleteTodo = deleteTodo
module.exports.updateTodo = updateTodo
