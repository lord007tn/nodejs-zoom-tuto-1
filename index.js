const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
// DB connection
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect('mongodb+srv://Raed:26765990@node-lfyij.mongodb.net/test?retryWrites=true&w=majority')
mongoose.connection.on('connected', ()=>{console.log("DB connected")})
mongoose.connection.on('error', (err)=>{console.log(err)})
// Import routes
const todoRoutes = require('./routes/todo.routes')

// Middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))

// Routes middleware
app.use('/todo', todoRoutes)
// Server listen
const port = 8000
app.listen(port, ()=>{
    console.log("el server yemchi jawou mezyan " + port)
})