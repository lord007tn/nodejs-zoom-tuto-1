const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TodoSchema = new Schema({
    title: {type: String, min:8 , max:255},
    description:{type: String}
})

module.exports = mongoose.model('Todo', TodoSchema)