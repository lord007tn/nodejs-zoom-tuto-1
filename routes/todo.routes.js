const router = require('express').Router()
const todoControllers = require('../controllers/todo.controllers')

router.get('/', todoControllers.getTodos)
router.get('/:id', todoControllers.getTodo)
router.post('/create', todoControllers.createTodo)
router.delete('/:id/delete', todoControllers.deleteTodo)
router.put('/:id/update', todoControllers.updateTodo)
module.exports = router
